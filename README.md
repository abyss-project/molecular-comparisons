# Notice

This material contains various config files to be used with [Abyss Pipeline Tool](https://gitlab.ifremer.fr/abyss-project/abyss-pipeline) to process data from:

ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/merlin/abyss/abyss-molecular-comparisons


# Reference

https://www.frontiersin.org/articles/10.3389/fmars.2020.00234/full